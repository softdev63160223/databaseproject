/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author focus
 */
public class ConnecttoDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffeeTWorrawit.db";
        try {
            conn = DriverManager.getConnection(url); //เชื่อมต่อกับjdbc แล้ว link กับ driver
            System.out.println("Connection to SQLite has been establish."); //ถ้าไม่มีจะสร้างใหม่เองโดยอัตโนมัติ
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally { //เมื่อเชื่อมต่อแล้วต้องปิดด้วย
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
}
