/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author focus
 */
public class InsertDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffeeTWorrawit.db";
        //Connect database
        try {
            conn = DriverManager.getConnection(url); //เชื่อมต่อกับjdbc แล้ว link กับ driver
            System.out.println("Connection to SQLite has been establish."); //ถ้าไม่มีจะสร้างใหม่เองโดยอัตโนมัติ
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return; // ทำอะไรต่อไม่ได้แล้ว
        }

        // Insert
        String sql = "INSERT INTO category(category_id, category_name) VALUES (?, ?)"; // ใส่เครื่องหมาย ? เพื่อจะได้ไม่ต้องใส่ "" ถ้าเป็น int
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "candy");
            int status = stmt.executeUpdate(); //ถ้าอัพเดทได้จะมีค่าเป็น int
          /*  ResultSet key = stmt.getGeneratedKeys(); // ดึงค่า key มาเก็บไว้
            key.next(); //เลื่อนไป 1resultset
            System.out.println(""+ key.getInt(1));*/

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
